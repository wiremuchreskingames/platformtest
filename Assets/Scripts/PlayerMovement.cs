﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float jumpHeight;
	public float moveAmount;

	private bool isJumping = false;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.RightArrow)) {
			transform.Translate (Vector2.right * moveAmount * Time.deltaTime);
			transform.eulerAngles = new Vector2 (0, 0);
		} 

		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.Translate (Vector2.right * moveAmount * Time.deltaTime);
			transform.eulerAngles = new Vector2 (0,180);

		} 
	
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (isJumping == false) {
				rigidbody2D.AddForce (Vector2.up * jumpHeight);
				isJumping = true;
			}
		}
	}

	void OnCollisionEnter2D () {
		isJumping = false;
	}
}
